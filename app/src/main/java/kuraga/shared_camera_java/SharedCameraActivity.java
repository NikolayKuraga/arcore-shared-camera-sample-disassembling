package kuraga.shared_camera_java;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.google.ar.core.CameraConfig;
import com.google.ar.core.CameraConfigFilter;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.SharedCamera;
import com.google.ar.core.exceptions.CameraNotAvailableException;

import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class SharedCameraActivity
    extends AppCompatActivity
    implements GLSurfaceView.Renderer // this class does not render things directly, but calls (sub)renderers' methods for rendering
{
  private static final String TAG = SharedCameraActivity.class.getSimpleName();

  // --- AppCompatActivity part --------------------------------------------------------------------

  private GLSurfaceView surfaceView;
  private final BackgroundRenderer backgroundRenderer = new BackgroundRenderer();

  private CameraManager cameraManager;
  private CameraDevice cameraDevice;

  // arcore things
  private Session sharedSession;
  private SharedCamera sharedCamera; // arcore camera for sharing got from "sharedSession"
  private String cameraId;

  private CaptureRequest.Builder previewCaptureRequestBuilder; // thing to request preview from "cameraDevice"
  private CameraCaptureSession captureSession;

  private ImageReader cpuImageReader; // image reader for arcore's CPU stream

  private HandlerThread backgroundThread;
  private Handler backgroundHandler;

  // triggers
  private boolean surfaceCreated;
  private boolean arMode = false;
  private boolean captureSessionChangesPossible = true;  // it is false after CameraManager.openCamera()
  private boolean arcoreActive; // indicates whether arcore is active now
  private final AtomicBoolean shouldUpdateSurfaceTexture = new AtomicBoolean(false); // true when new frames are available
  private final AtomicBoolean isFirstFrameWithoutArcore = new AtomicBoolean(true); // has user just entered non-ar mode?

  private final ConditionVariable safeToExitApp = new ConditionVariable(); // very important thing

  private final CameraCaptureSession.CaptureCallback cameraCaptureCallback =
      new CameraCaptureSession.CaptureCallback() {

        @Override
        public void onCaptureCompleted(
            CameraCaptureSession session,
            CaptureRequest request,
            TotalCaptureResult result) {
          shouldUpdateSurfaceTexture.set(true);
        }

        @Override
        public void onCaptureBufferLost(
            CameraCaptureSession session,
            CaptureRequest request,
            Surface target,
            long frameNumber) {
          Log.e(TAG, "onCaptureBufferLost: " + frameNumber);
          finishAndRemoveTask();
        }

        @Override
        public void onCaptureFailed(
            CameraCaptureSession session,
            CaptureRequest request,
            CaptureFailure failure) {
          Log.e(TAG, "onCaptureFailed: " + failure.getFrameNumber() + " " + failure.getReason());
          finishAndRemoveTask();
        }

        @Override
        public void onCaptureSequenceAborted(CameraCaptureSession session, int sequenceId) {
          Log.e(TAG, "onCaptureSequenceAborted: " + sequenceId + " " + session);
          finishAndRemoveTask();
        }
      };

  private void resumeArcore() {
    if(sharedSession == null || arcoreActive) {
      return;
    }

    try {
      sharedSession.resume();
      arcoreActive = true;
      sharedCamera.setCaptureCallback(cameraCaptureCallback, backgroundHandler);
    } catch (CameraNotAvailableException e) {
      Log.e(TAG, "Failed to resume arcore session", e);
      finishAndRemoveTask();
    }
  }

  private void pauseArcore() {
    if(arcoreActive) {
      sharedSession.pause();
      isFirstFrameWithoutArcore.set(true);
      arcoreActive = false;
    }
  }

  private void setRepeatingCaptureRequest() {
    try {
      // set camera effects if desire
      // {...}

      captureSession.setRepeatingRequest(
          previewCaptureRequestBuilder.build(), cameraCaptureCallback, backgroundHandler);
    } catch (CameraAccessException e) {
      Log.e(TAG, "Failed to set repeating request", e);
      finishAndRemoveTask();
    }
  }

  private void resumeCamera2() {
    setRepeatingCaptureRequest();
    sharedCamera.getSurfaceTexture()
        .setOnFrameAvailableListener(surfaceTexture -> {
//          Log.i(TAG, "Frame available.");
        });
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    surfaceView = findViewById(R.id.glsurfaceview);
    surfaceView.setPreserveEGLContextOnPause(true); // helps to improve performance and get rid of some errors
    surfaceView.setEGLContextClientVersion(2);
    surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
    surfaceView.setRenderer(this);
    surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

    Switch arcoreSwitch = findViewById(R.id.arcore_switch);
    arcoreSwitch.setChecked(arMode);
    arcoreSwitch.setOnCheckedChangeListener(
        (view, checked) -> {
          Log.d(TAG, "Switching to " + (checked ? "ar" : "non-ar") + " mode.");
          arMode = checked;
          if (arMode) {
            resumeArcore();
          } else {
            pauseArcore();
            resumeCamera2();
          }
        });
  }

  @Override
  protected void onDestroy() {
    if(sharedSession != null) {
      sharedSession.close();
      sharedSession = null;
    }
    super.onDestroy();
  }

  private synchronized void waitUntilCameraCaptureSessionIsActive() {
    while (!captureSessionChangesPossible) {
      try {
        this.wait();
      } catch (InterruptedException e) {
        Log.e(TAG, "Unable to wait for a safe time to make changes to the capture session", e);
        finishAndRemoveTask();
      }
    }
  }

  private final CameraCaptureSession.StateCallback cameraSessionStateCallback =
      new CameraCaptureSession.StateCallback() {

        // called when the camera capture session is configured first time or activity is resumed
        @Override
        public void onConfigured(CameraCaptureSession session) {
          Log.d(TAG, "Camera capture session configured.");
          captureSession = session;
          if (arMode) {
            setRepeatingCaptureRequest();
            // resumeArcore(); // is called withing "onActive" callback function
          } else {
            // setRepeatingCaptureRequest(); // is called withing "resumeCamera2"
            resumeCamera2();
          }
        }

        @Override
        public void onSurfacePrepared(CameraCaptureSession session, Surface surface) {
          Log.d(TAG, "Camera capture surface prepared.");
        }

        @Override
        public void onReady(CameraCaptureSession session) {
          Log.d(TAG, "Camera capture session ready.");
        }

        @Override
        public void onActive(CameraCaptureSession session) {
          Log.d(TAG, "Camera capture session active.");
          if (arMode && !arcoreActive) {
            resumeArcore();
          }
          synchronized (SharedCameraActivity.this) {
            captureSessionChangesPossible = true;
            SharedCameraActivity.this.notify();
          }
        }

        @Override
        public void onCaptureQueueEmpty(CameraCaptureSession session) {
          Log.w(TAG, "Camera capture queue empty.");
        }

        @Override
        public void onClosed(CameraCaptureSession session) {
          Log.d(TAG, "Camera capture session closed.");
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
          Log.e(TAG, "Failed to configure camera capture session.");
          finishAndRemoveTask();
        }
      };

  private final CameraDevice.StateCallback cameraDeviceCallback =
      new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
          Log.d(TAG, "Camera device ID " + cameraDevice.getId() + " opened.");
          SharedCameraActivity.this.cameraDevice = cameraDevice;
          try {
            sharedSession.setCameraTextureName(backgroundRenderer.getTextureId());
            sharedCamera.getSurfaceTexture().setOnFrameAvailableListener(surfaceTexture -> {
//              Log.i(TAG, "Frame available.");
            });

            previewCaptureRequestBuilder =
                cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);

            // here we create list of surfaces -- "surfaceList":
            //   0. sharedCamera.getSurfaceTexture()
            //   1. ...
            //   2. cpuImageReader.getSurface()
            List<Surface> surfaceList = sharedCamera.getArCoreSurfaces();
            surfaceList.add(cpuImageReader.getSurface());
            for(Surface surface : surfaceList) {
              previewCaptureRequestBuilder.addTarget(surface);
            }

            CameraCaptureSession.StateCallback wrappedCallback =
                sharedCamera.createARSessionStateCallback(cameraSessionStateCallback, backgroundHandler);
            cameraDevice.createCaptureSession(surfaceList, wrappedCallback, backgroundHandler);
          } catch (CameraAccessException e) {
            Log.e(TAG, "CameraAccessException", e);
            finishAndRemoveTask();
          }
        }

        @Override
        public void onClosed(CameraDevice cameraDevice) {
          Log.d(TAG, "Camera device ID " + cameraDevice.getId() + " closed.");
          SharedCameraActivity.this.cameraDevice = null;
          safeToExitApp.open();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
          Log.e(TAG, "Camera device ID " + cameraDevice.getId() + " disconnected.");
          cameraDevice.close();
          SharedCameraActivity.this.cameraDevice = null;
          finishAndRemoveTask();
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
          Log.e(TAG, "Camera device ID " + cameraDevice.getId() + " error " + error);
          cameraDevice.close();
          SharedCameraActivity.this.cameraDevice = null;
          finishAndRemoveTask();
        }
      };

  private void openCamera() {
    if (cameraDevice != null) {
      return;
    }

    if(!CameraPermissionHelper.hasCameraPermission(this)) {
      CameraPermissionHelper.requestCameraPermission(this);
      return;
    }

    // check arcore status here

    if (sharedSession == null) {
      try {
        sharedSession = new Session(this, EnumSet.of(Session.Feature.SHARED_CAMERA));
      } catch (Exception e) {
        Log.e(TAG, "Failed to create arcore session that supports camera sharing", e);
        finishAndRemoveTask();
      }

      // enable auto focus for sharedSession
      Config configSession = sharedSession.getConfig();
      configSession.setFocusMode(Config.FocusMode.AUTO);
      sharedSession.configure(configSession);

      { // change camera { config / texture resolution / image resolution / fps range }
        CameraConfigFilter cameraConfigFilter = new CameraConfigFilter(sharedSession); // filter for configs
        cameraConfigFilter.setTargetFps(EnumSet.of(CameraConfig.TargetFps.TARGET_FPS_30)); // filter not 30fps configs
        List<CameraConfig> listConfig =
            sharedSession.getSupportedCameraConfigs(cameraConfigFilter); // create list of available configs acceptable for specified filter

        { // there are only logs here
          StringBuilder sb = new StringBuilder("Available Arcore camera configs:");
          for (CameraConfig config : listConfig) {
            sb.append("\nconfig:")
                .append("\nCAMERA TEXTURE (sent to gpu) RESOLUTION -- ")
                .append(config.getTextureSize())
                .append("\nCAMERA IMAGE   (sent to cpu) RESOLUTION -- ")
                .append(config.getImageSize());
          }
          Log.i(TAG, sb.toString());
        }

        // select camera config with the largest GPU resolution and 640x480 CPU resolution
        CameraConfig configBest = listConfig.get(0);
        for (CameraConfig config : listConfig) { // first config is used by Arcore by default
          if (configBest.getTextureSize().getWidth() <= config.getTextureSize().getWidth()
              && config.getImageSize().equals(new Size(640, 480))) {
            configBest = config;
          }
        }
        Log.i(TAG, "This config will be used:"
            + "\nCAMERA TEXTURE (sent to gpu) RESOLUTION -- "
            + configBest.getTextureSize()
            + "\nCAMERA IMAGE   (sent to cpu) RESOLUTION -- "
            + configBest.getImageSize());

        sharedSession.setCameraConfig(configBest);
      }
    }

    sharedCamera = sharedSession.getSharedCamera();
    cameraId = sharedSession.getCameraConfig().getCameraId();

    // use just set up CPU image reader
    Size desiredCpuImageSize = sharedSession.getCameraConfig().getImageSize();
    cpuImageReader = ImageReader.newInstance(
        desiredCpuImageSize.getWidth(),
        desiredCpuImageSize.getHeight(),
        ImageFormat.YUV_420_888,
        2);
    cpuImageReader.setOnImageAvailableListener(
        imageReader -> {
          Image image = imageReader.acquireLatestImage();
          image.close();
        },
        backgroundHandler);
    sharedCamera.setAppSurfaces(this.cameraId, Collections.singletonList(cpuImageReader.getSurface()));

    try {
      CameraDevice.StateCallback wrappedCallback =
          sharedCamera.createARDeviceStateCallback(cameraDeviceCallback, backgroundHandler);
      cameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
      captureSessionChangesPossible = false;
      cameraManager.openCamera(cameraId, wrappedCallback, backgroundHandler);
    } catch (CameraAccessException | IllegalArgumentException | SecurityException e) {
      Log.e(TAG, "Failed to open camera", e);
      finishAndRemoveTask();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    waitUntilCameraCaptureSessionIsActive();
    // start some background thread which will work with callbacks
    backgroundThread = new HandlerThread("sharedCameraBackground");
    backgroundThread.start();
    backgroundHandler = new Handler(backgroundThread.getLooper());
    surfaceView.onResume();

    if(surfaceCreated) {
      openCamera();
    }
  }

  @Override
  public void onPause() {
    shouldUpdateSurfaceTexture.set(false);
    surfaceView.onPause();
    waitUntilCameraCaptureSessionIsActive();
    if (arMode) {
      pauseArcore();
    }

    // close camera
    if (captureSession != null) {
      captureSession.close();
      captureSession = null;
    }
    if (cameraDevice != null) {
      waitUntilCameraCaptureSessionIsActive();
      safeToExitApp.close();
      cameraDevice.close();
      safeToExitApp.block();
    }
    if (cpuImageReader != null) {
      cpuImageReader.close();
      cpuImageReader = null;
    }

    // stop background thread
    if (backgroundThread != null) {
      backgroundThread.quitSafely();
      try {
        backgroundThread.join();
        backgroundThread = null;
        backgroundHandler = null;
      } catch (InterruptedException e) {
        Log.e(TAG, "Interrupted while trying to join background handler thread", e);
        finishAndRemoveTask();
      }
    }
    super.onPause();
  }

  // --- GLSurfaceView.Renderer part -----------------------------------------------------------------
  // all GL surface callback functions are supposed to called on separated (OpenGL?) thread

  private int screenWidth;
  private int screenHeight;

  @Override
  public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    surfaceCreated = true;
    GLES20.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

    try { // prepare different necessary things for rendering
      backgroundRenderer.createOnGlThread(this);
      openCamera();
    } catch (IOException e) { // "createOnGlThread" supposed to read shaders source files
      Log.e(TAG, "Failed to read an asset file", e);
      finishAndRemoveTask();
    }
  }

  @Override
  public void onSurfaceChanged(GL10 gl, int width, int height) {
    GLES20.glViewport(0, 0, width, height);
    screenWidth = width;
    screenHeight = height;
    sharedSession.setDisplayGeometry(Surface.ROTATION_0, screenWidth, screenHeight);
  }

  public void onDrawFrameArcore() throws CameraNotAvailableException {
    if (!arcoreActive) {
      return;
    }
    Frame frame = sharedSession.update(); // update TEXTURE_EXTERNAL_OES
    backgroundRenderer.draw(frame); // draw TEXTURE_EXTERNAL_OES

    // save texture (image from GPU thread)
  }

  public void onDrawFrameCamera2() {
    SurfaceTexture texture = sharedCamera.getSurfaceTexture();
    if (isFirstFrameWithoutArcore.getAndSet(false)) {
      try {
        texture.detachFromGLContext();
      } catch(RuntimeException e) { // nothing to worry about if this exception is thrown several times
        Log.w(TAG, "Failed to detach texture from GL context", e);
      }
      texture.attachToGLContext(backgroundRenderer.getTextureId());
    }
    texture.updateTexImage();
    Size size = sharedSession.getCameraConfig().getTextureSize();
    backgroundRenderer.draw(size.getWidth(), size.getHeight(), screenWidth, screenHeight);
  }

  @Override
  public void onDrawFrame(GL10 gl) {
    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
    if (!shouldUpdateSurfaceTexture.get()) {
      return;
    }

    try {
      if (arMode) {
        onDrawFrameArcore();
      } else {
        onDrawFrameCamera2();
      }
    } catch (Throwable t) {
      Log.e(TAG, "Exception on the OpenGL thread", t);
      finishAndRemoveTask();
    }
  }
}
