package kuraga.shared_camera_java;

import android.content.Context;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.util.Log;

import com.google.ar.core.Coordinates2d;
import com.google.ar.core.Frame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class BackgroundRenderer {

  private static final String TAG = BackgroundRenderer.class.getSimpleName();
  private static final int FLOAT_SIZE = 4;

  // shaders, shader programs and other shader stuff
//    private static final String NAME_SHADER_VERTEX = "shaders/shader.vert";
//    private static final String NAME_SHADER_FRAGMENT = "shaders/fragment.vert";
  private int cameraProgram;
  private int cameraTextureUniform;

  // buffers' things
  private FloatBuffer quadCoords;
  private FloatBuffer quadTexCoords;
  private static final int COORDS_PER_VERTEX = 2;
  private static final int TEXCOORDS_PER_VERTEX = 2;
  private int cameraPositionAttrib;
  private int cameraTexCoordAttrib;
  private int cameraTextureId = -1;

  public int getTextureId() {
    return cameraTextureId;
  }

  public void createOnGlThread(Context context) throws IOException {
    int[] textures = {0};
    GLES20.glGenTextures(1, textures, 0);
    cameraTextureId = textures[0];
    int textureTarget = GLES11Ext.GL_TEXTURE_EXTERNAL_OES;
    GLES20.glBindTexture(textureTarget, cameraTextureId);
    GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
    GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
    GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
    GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

    int numVertices = 4;
    if (numVertices != QUAD_COORDS.length / COORDS_PER_VERTEX) {
      throw new RuntimeException("Unexpected number of vertices in BackgroundRenderer.");
    }

    ByteBuffer bbCoords = ByteBuffer.allocateDirect(QUAD_COORDS.length * FLOAT_SIZE);
    bbCoords.order(ByteOrder.nativeOrder());
    quadCoords = bbCoords.asFloatBuffer();
    quadCoords.put(QUAD_COORDS);
    quadCoords.position(0);

    ByteBuffer bbTexCoordsTransformed =
        ByteBuffer.allocateDirect(numVertices * TEXCOORDS_PER_VERTEX * FLOAT_SIZE);
    bbTexCoordsTransformed.order(ByteOrder.nativeOrder());
    quadTexCoords = bbTexCoordsTransformed.asFloatBuffer();

    { // shader program things (load shaders, link, compile, so on)
      int shaderVert, shaderFrag;
      final int[] statusCompileShaderVert = {0}, statusCompileShaderFrag = {0};
      shaderVert = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
      GLES20.glShaderSource(shaderVert, codeShaderVert);
      GLES20.glCompileShader(shaderVert);
      GLES20.glGetShaderiv(shaderVert, GLES20.GL_COMPILE_STATUS, statusCompileShaderVert, 0);
      if (statusCompileShaderVert[0] == 0) {
        Log.e(TAG, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shaderVert));
        throw new RuntimeException("Error creating shader");
      }
      shaderFrag = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
      GLES20.glShaderSource(shaderFrag, codeShaderFrag);
      GLES20.glCompileShader(shaderFrag);
      GLES20.glGetShaderiv(shaderFrag, GLES20.GL_COMPILE_STATUS, statusCompileShaderFrag, 0);
      if (statusCompileShaderFrag[0] == 0) {
        Log.e(TAG, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shaderFrag));
        throw new RuntimeException("Error creating shader");
      }

      cameraProgram = GLES20.glCreateProgram();
      GLES20.glAttachShader(cameraProgram, shaderVert);
      GLES20.glAttachShader(cameraProgram, shaderFrag);
      GLES20.glLinkProgram(cameraProgram);
      GLES20.glUseProgram(cameraProgram);
      cameraPositionAttrib = GLES20.glGetAttribLocation(cameraProgram, "a_Position");
      cameraTexCoordAttrib = GLES20.glGetAttribLocation(cameraProgram, "a_TexCoord");
      cameraTextureUniform = GLES20.glGetUniformLocation(cameraProgram, "sTexture");
    }
  }

  private void draw() {
    quadTexCoords.position(0);

    GLES20.glDisable(GLES20.GL_DEPTH_TEST); // well, ok, whatever
    GLES20.glDepthMask(false); // well, ok, whatever

    GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

    GLES20.glUseProgram(cameraProgram);
    GLES20.glUniform1i(cameraTextureUniform, 0);

    GLES20.glVertexAttribPointer(
        cameraPositionAttrib, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, quadCoords);
    GLES20.glVertexAttribPointer(
        cameraTexCoordAttrib, TEXCOORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, quadTexCoords);
    GLES20.glEnableVertexAttribArray(cameraPositionAttrib);
    GLES20.glEnableVertexAttribArray(cameraTexCoordAttrib);
    GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4); // here we draw
    GLES20.glDisableVertexAttribArray(cameraPositionAttrib); // is not necessary to call...
    GLES20.glDisableVertexAttribArray(cameraTexCoordAttrib); //...but to call would be safer
  }

  public void draw(Frame frame) {
    if (frame.hasDisplayGeometryChanged()) {
      frame.transformCoordinates2d(
          Coordinates2d.OPENGL_NORMALIZED_DEVICE_COORDINATES,
          quadCoords,
          Coordinates2d.TEXTURE_NORMALIZED,
          quadTexCoords);
    }

    if (frame.getTimestamp() == 0) {
      return;
    }

    draw();
  }

  public void draw(int imageWidth, int imageHeight, int screenWidth, int screenHeight) {
    float imageAspectRatio = (float) imageWidth / imageHeight; // because image is landscape...
    float screenAspectRatio = (float) screenHeight / screenWidth; //...and screen is portrait?

    float croppedWidth;
    float croppedHeight;
    if (screenAspectRatio < imageAspectRatio) {
      croppedWidth = imageHeight * screenAspectRatio;
      croppedHeight = imageHeight;
    } else {
      croppedWidth = imageWidth;
      croppedHeight = imageWidth / screenAspectRatio;
    }

    float u = (imageWidth - croppedWidth) / imageWidth * 0.5f;
    float v = (imageHeight - croppedHeight) / imageHeight * 0.5f;

    float[] texCoordTransformed = { // if fullscreen and camera ratio equals display one, then
        1 - u, 1 - v,                 // (1.0f, 1.0f)
        1 - u, v,                     // (1.0f, 0,0f)
        u, 1 - v,                 // (0.0f, 1.0f)
        u, v                      // (0.0f, 0.0f)
    };

    quadTexCoords.put(texCoordTransformed);
    draw();
  }

  private static final float[] QUAD_COORDS = new float[]{
      -1.0f, -1.0f,
      1.0f, -1.0f,
      -1.0f, 1.0f,
      1.0f, 1.0f,
  };

  private static final String codeShaderFrag =
      "#extension GL_OES_EGL_image_external : require" + "\n"
          + "precision mediump float;" + "\n"
          + "varying vec2 v_TexCoord;" + "\n"
          + "uniform samplerExternalOES sTexture;" + "\n"
          + "void main() {" + "\n"
          + "  gl_FragColor = texture2D(sTexture, v_TexCoord);" + "\n"
          + "}" + "\n";

  private static final String codeShaderVert =
      "attribute vec4 a_Position;" + "\n"
          + "attribute vec2 a_TexCoord;" + "\n"
          + "varying vec2 v_TexCoord;" + "\n"
          + "void main() {" + "\n"
          + "  gl_Position = a_Position;" + "\n"
          + "  v_TexCoord = a_TexCoord;" + "\n"
          + "}" + "\n";
}
