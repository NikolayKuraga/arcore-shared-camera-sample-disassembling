package kuraga.shared_camera_java;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.ar.core.ArCoreApk;
import com.google.ar.core.exceptions.UnavailableException;

public class ArcoreChecker {

  private static final String TAG = ArcoreChecker.class.getSimpleName();
  private final Context context;

  public ArcoreChecker(Context context) {
    this.context = context;
  }

  public boolean isArcoreSupportedAndUpToDate() {
    ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(context);
    switch (availability) {
      case SUPPORTED_INSTALLED:
        break;
      case SUPPORTED_APK_TOO_OLD:
      case SUPPORTED_NOT_INSTALLED:
        try {
          ArCoreApk.InstallStatus installStatus =
              ArCoreApk.getInstance().requestInstall(
                  (Activity) context, true);
          switch (installStatus) {
            case INSTALL_REQUESTED:
              Log.e(TAG, "Arcore installation requested.");
              return false;
            case INSTALLED:
              break;
          }
        } catch (UnavailableException e) {
          Log.e(TAG, "Arcore not installed", e);
          return false;
        }
        break;
      case UNKNOWN_ERROR:
      case UNKNOWN_CHECKING:
      case UNKNOWN_TIMED_OUT:
      case UNSUPPORTED_DEVICE_NOT_CAPABLE:
        Log.e(
            TAG,
            "Arcore is not supported on this device, ArCoreApk.checkAvailability() returned "
                + availability);
        return false;
    }
    return true;
  }
}
